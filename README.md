# asif

"As if" is a toy stack-based language interpreter implemented with Clojure's `transduce` using transducers for the `lex`, `parse`, and `optimize` phases.

Would you implement a serious language this way? As if!

I put this minimal program together to demonstrate the implementation and composition of Clojure transducers. The entry point `semperos.asif/interpret` function is defined as follows:

```clojure
(defn interpret
  "Evaluate the `program` string, return the resultant stack of values."
  ([program] (interpret program evaluate))
  ([program reducing-function]
   (transduce (comp lex parse optimize) reducing-function program)))
```

Additional things I found satisfying:

* Most of the built-ins of the language defer directly to Clojure functions; asif passes args to them from the stack.
* The `i.` and `a.` words are inspired by J's [`i.`](https://code.jsoftware.com/wiki/Vocabulary/idot) and [`a.`](https://code.jsoftware.com/wiki/Vocabulary/adot)
* There is _syntax_ for `map`, `apply`, and `reduce` capabilities (see examples below)

Notable things _not_ supported at this time:

* New word definition
* Quotations

## Usage

```clojure
semperos.asif=> (interpret "3 4 + 5 +")
[12]

semperos.asif=> (interpret "\"clojure\" upper")
["CLOJURE"]

semperos.asif=> (interpret "65 a.")
[\A]

semperos.asif=> (interpret "5 i.")
[(0 1 2 3 4)]

semperos.asif=> (interpret "5 i. +///") ;; reduce
[10]

semperos.asif=> (interpret "5 i. inc***") ;; map
[(1 2 3 4 5)]

semperos.asif=> (interpret "5 i. inc*** inc***")
[(2 3 4 5 6)]

semperos.asif=> (clojure.pprint/pprint (sort built-ins))
(["*" [#'clojure.core/* 2]]
 ["+" [#'clojure.core/+ 2]]
 ["-" [#'clojure.core/- 2]]
 ["/" [#'clojure.core// 2]]
 ["a." [#'semperos.asif/alphabet 1]]
 ["dec" [#'clojure.core/dec 1]]
 ["drop" [#'semperos.asif/drop1 1]]
 ["drop2" [#'semperos.asif/drop2 2]]
 ["i." [#'clojure.core/range 1]]
 ["inc" [#'clojure.core/inc 1]]
 ["lower" [#'clojure.string/lower-case 1]]
 ["upper" [#'clojure.string/upper-case 1]])
```

## License

Copyright © 2020–2022 Daniel Gregoire

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, you can obtain one at http://mozilla.org/MPL/2.0/.
