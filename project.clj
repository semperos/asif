(defproject semperos/asif "0.1.0-SNAPSHOT"
  :description "Language implemented in a single Clojure transducer"
  :url "https://gitlab.com/semperos/asif"
  :license {:name "Mozilla Public License Version 2.0"
            :url "http://mozilla.org/MPL/2.0/"}
  :main ^:skip-aot semperos.asif
  :target-path "target/%s"
  :dependencies [[org.clojure/clojure "1.11.1"]])
