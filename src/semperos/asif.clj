(ns semperos.asif
  (:require [clojure.string :as str])
  (:gen-class))

;;;;;;;;;;;;;
;; Helpers ;;
;;;;;;;;;;;;;

(def space? #{\space \return \newline \tab \,})

(defn peek-args
  [stack n]
  (reduce
   (fn [[stack args] _]
     (let [arg (peek stack)
           stack (pop stack)]
       [stack (conj args arg)]))
   [stack []]
   (range n)))

(defn throw-unsupported-word [form]
  (throw (ex-info (str "Unsupported word: " form)
                  {:unsupported-word form})))

(defn try-literal [form]
  (try
    [(read-string form) 0]
    (catch Exception _e
      (throw-unsupported-word form))))

(defn drop1 [_a]
  ::nil)

(defn drop2 [_a _b]
  ::nil)

(defn zero2 [_a _b]
  0)

(defn push [stack x]
  (if (= ::nil x)
    stack
    (conj stack x)))

(def alphabet (mapv char (range 256)))

(def built-ins
  {"+"     [#'+ 2]              "*"     [#'* 2]
   "-"     [#'- 2]              "/"     [#'/ 2]
   "inc"   [#'inc 1]            "dec"   [#'dec 1]
   "upper" [#'str/upper-case 1] "lower" [#'str/lower-case 1]
   "drop"  [#'drop1 1]          "drop2" [#'drop2 2]
   "i."    [#'range 1]          "a."    [#'alphabet 1]})

(def apply-suffix "...")
(def apply-suffix-length (count apply-suffix))

(def map-suffix "***")
(def map-suffix-length (count map-suffix))

(def reduce-suffix "///")
(def reduce-suffix-length (count reduce-suffix))

(defn resolve-word
  "A word is a tuple of the Clojure fn and its arity that defines the
  word."
  [form]
  (let [[f arity] (get built-ins form)]
    (cond
      (and f arity)
      [f arity]

      (str/ends-with? form apply-suffix)
      (let [l (count form)
            sub-form (subs form 0 (- l apply-suffix-length))
            [f arity] (get built-ins sub-form)]
        (if (and f arity)
          [f ##Inf]
          (throw-unsupported-word form #_that-the-user-wrote)))

      (str/ends-with? form map-suffix)
      (let [l (count form)
            sub-form (subs form 0 (- l map-suffix-length))
            [f arity] (get built-ins sub-form)]
        (if (and f arity)
          [#(map f %) 1]
          (throw-unsupported-word form #_that-the-user-wrote)))

      (str/ends-with? form reduce-suffix)
      (let [l (count form)
            sub-form (subs form 0 (- l reduce-suffix-length))
            [f arity] (get built-ins sub-form)]
        (if (and f arity)
          [#(transduce (map identity) f %) 1]
          (throw-unsupported-word form #_that-the-user-wrote)))

      :else
      (try-literal form))))

(def optimizations
  {#'- [= [#'zero2 2]]})

;;;;;;;;;;;;;;;;;
;; Transducers ;;
;;;;;;;;;;;;;;;;;

(defn lex
  [rf]
  (let [buf (volatile! {:tokens []
                        :string nil})]
    (fn
      ([] (rf))
      ([result]
       (if (string? (:string @buf))
         (throw (ex-info "Lexer: Unfinished string value."
                         {:partial-string (:string @buf)}))
         (let [token (apply str (:tokens @buf))]
           (if (empty? token)
             result
             (rf result token)))))
      ([result input]
       (if (reduced? result)
         result
         (cond
           (string? (:string @buf))
           (if (= \" input)
             (let [string (str "\"" (:string @buf) "\"")]
               (vswap! buf (fn [buf]
                             (-> buf
                                 (assoc :string nil)
                                 (update :tokens conj string))))
               result)
             (do
               (vswap! buf update :string str input)
               result))

           (space? input)
           (let [token (apply str (:tokens @buf))]
             (vswap! buf assoc :tokens [])
             (rf result token))

           (= \" input)
           (do
             (vswap! buf assoc :string "")
             result)

           :else
           (do
             (vswap! buf update :tokens conj input)
             result)))))))

(def parse (map resolve-word))

(defn optimize
  [rf]
  (let [stack (volatile! [])]
    (fn
      ([] (rf))
      ([result] (rf result))
      ([result [word arity :as word-tuple]]
       (if (reduced? result)
         result
         (if-let [optm (get optimizations word)]
           (let [[stack* args] (peek-args @stack arity)
                 [optm-pred replacement] optm]
             (if (apply optm-pred args)
               (do
                 (vreset! stack (concat stack* replacement))
                 (rf result replacement))
               (do
                 (vswap! stack conj word-tuple)
                 (rf result word-tuple))))
           (do
             (vswap! stack conj word-tuple)
             (rf result word-tuple))))))))

;;;;;;;;;;;;;;;;;;;;;;;;
;; Reducing Functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(def evaluate
  "Reducing function (rf) for use with asif."
  (completing
   (fn
     ([] [])
     ([stack [f arity]]
      (cond
        (zero? arity) (conj stack f)
        (= ##Inf arity) (push [] (apply f stack))
        (= ##-Inf arity) (push [] (f stack))

        :else
        (let [[stack args] (peek-args stack arity)]
          (push stack (apply f args))))))))

;; TODO
(def debug
  (completing
   (fn
     ([] [])
     ([stack [f arity]]
      (cond
        (zero? arity) (conj stack f)
        (= ##Inf arity) (push [] (apply f stack))
        (= ##-Inf arity) (push [] (f stack))

        :else
        (let [[stack args] (peek-args stack arity)]
          (push stack (apply f args))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Program Entry-Points ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn interpret
  "Evaluate the `program` string, return the resultant stack of values."
  ([program] (interpret program evaluate))
  ([program reducing-function]
   (transduce (comp lex parse optimize) reducing-function program)))

(defn- print-results
  [interpreter-results]
  (let [r interpreter-results
        cnt (count r)]
    (if (= cnt 1)
      (prn (first r))
      (doseq [n (range cnt)]
        (println (str "Result #" (inc n) ": " (pr-str (nth r n))))))))

(defn -main
  [& args]
  (let [stdin (str/trim (slurp *in*))]
    (print-results
     (if-not (str/blank? stdin)
       (interpret stdin)
       (if-not (seq args)
         (binding [*out* *err*]
           (println "Usage:\n\n   asif <program-file>\n\n   asif -e <program-string>")
           (System/exit 1))
         (if (#{"-e" "--eval"} (first args))
           (interpret (second args))
           (interpret (slurp (first args)))))))))
