(ns semperos.asif-test
  (:require [clojure.test :refer [is deftest testing]]
            [semperos.asif :as nut]))

(deftest test-lex
  (is (= []
         (eduction nut/lex "")))
  (is (= ["a" "b" "c"]
         (eduction nut/lex "a b c")))
  (is (= ["a" "bc"]
         (eduction nut/lex "a bc")))
  (is (= ["\"a\"" "bc"]
         (eduction nut/lex "\"a\" bc"))))

(deftest test-parse
  (is (= []
         (eduction nut/parse [])))
  (is (= [[2 0] [3 0] [#'+ 2]]
         (eduction nut/parse ["2" "3" "+"]))))

(deftest test-optimize
  (is (= []
         (eduction nut/optimize [])))
  (is (= [[3 0] [3 0] [#'nut/zero2 2]]
         (eduction nut/optimize [[3 0] [3 0] [#'- 2]]))))

(deftest test-interpret
  (testing "Basics"
    (is (= [5]
           (nut/interpret "2 3 +"))))
  (testing "Optimization"
    (let [calls (atom 0)]
      (with-redefs [clojure.core/- (fn [& _args]
                                     (swap! calls inc))])
      (is (= [0]
             (nut/interpret "3 3 -")))
      (is (zero? @calls))))
  (testing "Apply syntax"
    (is (= [(* 1 2 3 4 5)]
           (nut/interpret "1 2 3 4 5 *..."))))
  (testing "Map syntax"
    (is (= [(map inc (range 10))]
           (nut/interpret "10 i. inc***"))))
  (testing "Builtins"
    (is (= [\F]
           (nut/interpret "70 a.")))))
